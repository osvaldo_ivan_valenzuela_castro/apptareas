import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  Widget createEmailInput(){
    return  Padding(
      padding: const EdgeInsets.only(top: 40),
      child: TextFormField(decoration: InputDecoration(hintText: 'Usuario o correo electronico'),
      ),
    );
  }
  Widget createPasswordInput(){
    return  Padding(
      padding: const EdgeInsets.only(top: 30),
      child: TextFormField(decoration: InputDecoration(hintText: 'Contraseña'),
      ),
    );
  }
  Widget createLoginBotton(){
    return Container(
        padding: const EdgeInsets.only(top:20),
        child:RaisedButton(
          child: Text('Entrar'),
          onPressed: (){} ,
      ));
  }
  Widget createAccountLink() {
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Text(
          'O crea tu cuenta aqui',
          textAlign: TextAlign.right,
          style: TextStyle(fontWeight: FontWeight.bold),
      ));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding:EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(color: Colors.white),
      child: ListView(children: [
        Image.asset(
          'assets/images/logo.png',
          height: 300,
        ),
        createEmailInput(),
        createPasswordInput(),
        createLoginBotton(),
        createAccountLink(),
      ]),
      )
    );
  }
}


